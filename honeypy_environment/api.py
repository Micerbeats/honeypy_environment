import re, os
from cerberus import Validator
from copy import deepcopy

from flask import Flask, Response, request
from flask_cors import CORS
from flask_basicauth import BasicAuth

from honeypy_environment import (REMOVE_VARIABLES, VARIABLE_SCHEMA, VARIABLES_SCHEMA, ENVIRONMENT_SCHEMA, ENVIRONMENT_SAVE_SCHEMA)
from honeypy.common import Common
from honeypy.database import Database
from honeypy.api import api

from pymongo.errors import DuplicateKeyError

"""
    Allow Cross origin requests while in development
"""
CORS(api, resources={r'\/?.*': {'origins': ['http://localhost:3000', 'http://localhost:4200', "http://frontend-service.default.svc.cluster.local", "http://honeypy-react.default.svc.cluster.local/"]}})

"""
    Instantiate Database, Common and Validator
"""
common = Common()
db = Database(api.config['DATABASE_IP'], api.config['DATABASE_PORT'], api.config['ENV_DB_NAME'], api.config['ENV_DB_COLLECTION'])
db.create_index("name")
validator = Validator({}, purge_unknown = True)
basic_auth = BasicAuth(api)

@api.route("/", methods = ["GET"])
@basic_auth.required
def get_environments():
    """
        Get all environments as list
    """
    environments = db.find({}, sort = True)
    return common.create_response(200, environments)

@api.route("/<env>", methods = ["GET"])
@basic_auth.required
def get(env):
    """
        Get an environment by name
    """
    document = db.find_one({"name":env})
    if document:
        return common.create_response(200, document)
    else:
        return common.create_response(404, {"name": "Environment does not exist"})


@api.route("/", methods = ["POST"])
@basic_auth.required
def create():
    """
        Create an environment
    """
    validator.schema = ENVIRONMENT_SCHEMA
    data = validator.normalized(request.get_json())
    validation = validator.validate(data)
    if validation == True:
        try:
            db.insert_one(data)
            return common.create_response(201)
        except DuplicateKeyError as error:
            field = re.search(r'(?:index: (.*)_1)', str(error)).groups()[0]
            value = re.search(r'(?:\{ : "(.*)" \})', str(error)).groups()[0]
            return common.create_response(409, {field: f"'{value}' already exists"})
    else:
        return common.create_response(400, validator.errors)


@api.route("/<env>", methods = ["PATCH"])
@basic_auth.required
def save(env):
    """
        Save an environment
    """
    validator.schema = ENVIRONMENT_SAVE_SCHEMA
    data = validator.normalized(request.get_json())
    validation = validator.validate(data)
    if validation == True:
        result = db.update_one({"name":env}, {"$set": data})
        if result.matched_count > 0:
            return common.create_response(204)
        else:
            return common.create_response(404, {"name": "Environment does not exist"})
    else:
        return common.create_response(400, validator.errors)

@api.route("/<env>/add", methods = ["PATCH"])
@basic_auth.required
def add_variable(env):
    """
        Add a variable to an environment
    """
    validator.schema = VARIABLES_SCHEMA
    data = validator.normalized(request.get_json())
    validation = validator.validate(data)
    if validation == True:
        variables = {}
        for variable in data["variables"]:
            variables["variables." + variable["name"]] = variable["value"]
        result = db.update_one({"name":env}, {"$set": variables})
        if result.matched_count > 0:
            return common.create_response(204)
        else:
            return common.create_response(404, {"name": "Environment does not exist"})
    else:
        return common.create_response(400, validator.errors)

@api.route("/<env>/remove", methods = ["PATCH"])
@basic_auth.required
def remove_variable(env):
    """
        Add a variable to an environment
    """
    validator.schema = REMOVE_VARIABLES
    data = validator.normalized(request.get_json())
    validation = validator.validate(data)
    if validation == True:
        variables = {}
        for variable in data["variables"]:
            variables["variables." + variable] = 1
        result = db.update_one({"name":env}, {"$unset":variables})
        if result.matched_count > 0:
            return common.create_response(204)
        else:
            return common.create_response(404, {"name": "Environment does not exist"})
    else:
        return common.create_response(400, validator.errors)

@api.route("/<env>", methods = ["DELETE"])
@basic_auth.required
def delete(env):
    """
        Delete a feature or phrase
    """
    result = db.delete_one({"name":env})
    if result.deleted_count > 0:
        return common.create_response(204)
    else:
        return common.create_response(404, {"name": "Environment does not exist"})

def main():
    if api.config["PRODUCTION"]:
        api.run(host=api.config["IP"], port=api.config["PORT"], threaded=True)
    else:
        api.run(host=api.config["ENV_IP"], port=api.config["ENV_PORT"], threaded=True)
