
# Honeypy Environment Service Tests

----

## Acceptance Criteria
* Verify the ability to create an environment
* Verify the ability to get an environment by it's name
* Verify the ability to save/update an environment
* Verify the ability to delete an environment

### Test Workflow:
  1. Setup test variables
  2. Get the environment first to verify it does not exist
  3. Create the environment
  4. Retrieve the newly created environment and verify it contains the correct data
  5. Save the environment with variables
  6. Verify the save updated the environment successfully
  7. Delete the environment
  8. Verify the environment deletion was a success
