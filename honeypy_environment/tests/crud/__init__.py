
ENVIRONMENT = {
    "name": "QA Test Environment"
}

VARIABLE_A = {
    "name": "variable_a",
    "value": "variable_a_value"
}

VARIABLE_B = {
    "name": "variable_b",
    "value": "variable_b_value"
}

VARIABLE_C = {
    "name": "variable_c",
    "value": "variable_c_value"
}

VARIABLES = {
    VARIABLE_A["name"]:VARIABLE_A["value"],
    VARIABLE_B["name"]:VARIABLE_B["value"],
    VARIABLE_C["name"]:VARIABLE_C["value"],
}
