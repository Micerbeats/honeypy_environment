import pytest, re
from copy import deepcopy
from honeypy.honeypy import Honeypy
from honeypy.common import Common

from honeypy_environment.tests.crud import ENVIRONMENT, VARIABLES
from honeypy.api.environment import EnvironmentService

def setup_module(module):
    """
        Setup test data
    """
    global environment
    global env_service
    global save_environment
    env_service = EnvironmentService()
    env_service.delete(ENVIRONMENT["name"])
    save_environment = deepcopy(ENVIRONMENT)
    save_environment["variables"] = VARIABLES

class TestEnvCrudRequests:

    def test_verify_404(self):
        """
            Verify the environment does not exist yet
            Verify a 404 status code is returned
        """
        response = env_service.get(ENVIRONMENT["name"])
        assert response.status_code == 404

    def test_create_environment(self):
        """
            Create a test environment
        """
        response = env_service.create(ENVIRONMENT)
        assert response.status_code == 201

    def test_get_environment(self):
        """
            Get the environment to verify it was created
        """
        response = env_service.get(ENVIRONMENT["name"])
        assert response.status_code == 200
        data = response.json()
        assert data["name"] == ENVIRONMENT["name"]
        assert data["variables"] == {}

    def test_save_environment(self):
        """
            Save the environment
            Add variables to the variables array
        """
        response = env_service.save(save_environment["name"], save_environment)
        assert response.status_code == 204

    def test_save_success(self):
        """
            Verify the environment save request persists and works
        """
        response = env_service.get(ENVIRONMENT["name"])
        assert response.status_code == 200
        data = response.json()
        assert data["variables"] == VARIABLES
        assert data["name"] == save_environment["name"]
        assert data["created"]
        assert data["modified"]

    def test_delete(self):
        """
            Verify the ability to delete an environment
        """
        response = env_service.delete(ENVIRONMENT["name"])
        assert response.status_code == 204

    def test_verify_404_again(self):
        """
            Verify the environment does not exist yet
            Verify a 404 status code is returned
        """
        response = env_service.get(ENVIRONMENT["name"])
        assert response.status_code == 404
