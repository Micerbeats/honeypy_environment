VARIABLE_SCHEMA = {
    "variable": {
        "type": "dict",
        "empty": False
    }
}

ENVIRONMENT_SCHEMA = {
    "name": {
        "type": "string",
        "required": True,
        "empty":False
    },
    "variables": {
        "type": "dict",
        "required": True,
        "default": {}
    }
}

ENVIRONMENT_SAVE_SCHEMA = {
    "name": {
        "type": "string",
        "empty":False
    },
    "variables": {
        "type": "dict",
        "required": True
    }
}

VARIABLES_SCHEMA = {
    "variables": {
        "type": "list",
        "required": True,
        "empty": False,
        "default": [],
        "schema": {
            "type":"dict",
            "empty": False,
            "schema": {
                "name": {
                    "type":"string",
                    "required":True,
                    "empty":False,
                    "regex": "([\w-]+)"
                },
                "value": {
                    "type":"string",
                    "required":True
                }
            }
        }
    }
}

REMOVE_VARIABLES = {
    "variables": {
        "type": "list",
        "required": True,
        "empty": False,
        "schema": {
            "type":"string",
            "empty": False
        }
    }
}
