from setuptools import setup

setup(
    name = "honeypy_environment",
    version = "0.1",
    description = "Honeypy Test Service",
    author = "Micah Prescott",
    author_email = "prescottmicah@gmail.com",
    packages = [
        "honeypy_environment",
        "honeypy_environment.tests",
        "honeypy_environment.tests.crud"
    ],
    install_requires = [
        "cerberus",
        "Flask",
        "Flask-Cors",
        "Flask-BasicAuth",
        "pymongo",
        "pytest",
        "requests"
    ],
    entry_points = {
        "console_scripts": [
            "honeypy_environment = honeypy_environment.api:main"
        ]
    }
)
